from spack import *
import os

class Basilisk(Package):
    """Basilisk is also the name of a Free Software program for the solution of partial differential equations on adaptive Cartesian meshes. It is destined to be the successor of Gerris and is developed by the same authors."""

    homepage = "http://basilisk.fr"
    url      = "http://basilisk.fr/basilisk/basilisk.tar.gz"

    maintainers = ['nvonwolf']

    version('latest', 'fb86c7e918524d52f3c6b620b72f92ad')

    # Base Depends
    depends_on('gmake', type='build')
    depends_on('gcc@:9', type=('build', 'link', 'run'))
    depends_on('flex', type=('build','link'))
    depends_on('bison', type=('build','link'))
    depends_on('gawk', type=('build','link'))

    # config.gcc mentioned depends
    depends_on('valgrind', type=('build','link'))
    depends_on('gnuplot +cairo', type=('build','link'))

    # Visualisation Depends
    depends_on('mesa +osmesa +glx +opengl +opengles', type=('build','link'))
    depends_on('mesa-glu', type=('build','link'))

    # Python Integration Depends
    depends_on('swig', type=('build','link'))
    depends_on('python', type=('build','link', 'run'))

    parallel = False
    phases = ['build']

    def build(self, spec, prefix):
        # Copy Source To Install (moving after build breaks things)
        install_tree('src', prefix)

        # Setup Build Environment
        env['BASILISK'] = prefix
        env['PATH'] = prefix + ':' + env['PATH']

        # Link Config File
        ln = which('ln')
        ln('-sr', join_path(prefix, 'config.gcc'), join_path(prefix, 'config'))

        # Edit Config File
        config = FileFilter(join_path(prefix, 'config.gcc'))
        config.filter('\$\(CC\)', self.spec['gcc'].prefix.bin.gcc)
        config.filter('PYTHONINCLUDE =.*', 'PYTHONINCLUDE = ' + self.spec['python'].headers.directories[0])
        config.filter('OPENGLIBS = -lfb_dumb', '#OPENGLIBS = -lfb_dumb')
        config.filter('CFLAGS \+= -DDUMBGL', 'CFLAGS += -Wno-unused-function')
        config.filter('# OPENGLIBS = -lfb_osmesa -lGLU -lOSMesa', 'OPENGLIBS = -lfb_osmesa -lGLU -lOSMesa')
        # Build
        with working_dir(join_path(prefix, 'gl')):
            make('libglutils.a', 'libfb_osmesa.a')
        with working_dir(prefix):
            make('-k','qcc', 'qplot', 'libkdt', 'literatec', 'libfb_dumb', 'libws')
            make('-k', 'draw_get.h', 'draw_json.h')
            make('-k')
            make()

    def setup_run_environment(self, env):
        env.prepend_path('PATH', self.prefix)
        env.set('BASILISK', self.prefix)
        env.set('BASILISK_INCLUDE_PATH', self.prefix)

