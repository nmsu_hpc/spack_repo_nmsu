from spack import *

class Hyp2mat(AutotoolsPackage):
    """hyp2mat is a utility which converts HyperLynx and QucsStudio pcb files to matlab for electromagnetic simulation, or to PDF for printing."""

    homepage = "https://github.com/koendv/hyp2mat"
    url      = "https://github.com/koendv/hyp2mat/archive/v0.0.18.tar.gz"

    maintainers = ['nvonwolf']

    version('0.0.18', sha256='f8df1dbb81409c2d450a8dc8c517f17e474bfc6ff8bbc5875a71b30e03915524')
    version('0.0.17', sha256='f3604f8ca0e98722553aa7a30a6fcd5324f2f39345ee21cda14df364d049584d')
    version('0.0.15', sha256='9880c3c297bd50b9a5c87402908704b1ba2c6cf16ca18d609bf8cbf98166e837')

    variant('library', default=True, description='Build the hyp2mat library, includes, and package info')
    variant('shared', default=True, description='Build shared libraries')

    # Class Dependencies
    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')
    depends_on('m4', type='build')

    # Build Dependencies
    depends_on('libharu', type=('build','link'))
    depends_on('libpng', type=('build','link'))
    depends_on('zlib', type=('build','link'))

    # Build Documentation Dependencies
    depends_on('gengetopt', type='build')
    depends_on('help2man', type='build')
    depends_on('groff', type='build')
    depends_on('ghostscript', type='build')

    # Other Dev Deps
    depends_on('libtool', type=('build'))
    #depends_on('make', type=('build'))
    depends_on('bison', type=('build','link'))
    depends_on('flex', type=('build','link'))

    def autoreconf(self, spec, prefix):
        mkdir = which('mkdir')
        mkdir(join_path(self.stage.source_path, 'm4'))
        autoreconf('-i')

    def configure_args(self):
        args = []

        if '+library' in self.spec:
            args.append('--enable-library')

        if '+shared' in self.spec:
            args.append('--enable-shared')

        return args

    def setup_run_environment(self, env):
        env.prepend_path('MATLABPATH', self.prefix.share.hyp2mat.matlab)

