from spack import *


class Openems(CMakePackage):
    """openEMS is a free and open electromagnetic field solver using the FDTD method"""

    homepage = "https://openems.de/start"
    url      = "https://github.com/thliebig/openEMS-Project/archive/v0.0.35.tar.gz"
    git      = "https://github.com/thliebig/openEMS-Project.git"

    maintainers = ['nvonwolf']

    version('master', branch='master', submodules=True)
    version('stable', branch='stable', submodules=True)
    version('0.0.35', tag='v0.0.35', submodules=True)

    variant('gui', default=False, description='Build QCSXCAD and AppCSXCAD')
    variant('ctb', default=True, description='Install Circuit Toolbox for Matlab/Octave')
    variant('mpi', default=False, description='Add MPI Abilities')
    variant('octave', default=True, description='Build Octave support')
    variant('hyp2mat', default=True, description='Support for HyperLynx/QucsStudio to PDF')

    depends_on('cmake@2.8:', type='build')
    depends_on('git', type='build')

    depends_on('hdf5 +hl', type=('build','link'))
    depends_on('vtk@8:8.999.999', type=('build','link'))
    depends_on('boost@1.69.0 +shared +thread ~timer', type=('build','link'))
    depends_on('cgal', type=('build','link'))
    depends_on('tinyxml', type=('build','link'))

    # GUI
    #depends_on('qt@4:4.999.999', type=('build','link'), when='+gui')
    depends_on('qt@5:', type=('build','link'), when='+gui')

    # Octave
    depends_on('octave', type=('build','link','run'), when='+octave')

    # Hyp2mat
    depends_on('hyp2mat', type=('build','link','run'), when='+hyp2mat')

    # MPI
    depends_on('openmpi', type=('build','link','run'), when='+mpi')

    def cmake_args(self):
        args = []

        if self.spec.satisfies('~gui'):
            args.append('-DBUILD_APPCSXCAD=NO')

        return args

    install_targets = []

    @run_after('install')
    def install_ctb(self):
        if self.spec.satisfies('+ctb'):
            install_tree(join_path(self.stage.source_path, 'CTB'), self.prefix.share.CTB.matlab)

    def setup_run_environment(self, env):
        env.prepend_path('MATLABPATH', self.prefix.share.openEMS.matlab)
        env.prepend_path('MATLABPATH', self.prefix.share.CSXCAD.matlab)

        if self.spec.satisfies('+ctb'):
            env.prepend_path('MATLABPATH', self.prefix.share.CTB.matlab)

