from spack import *
import os.path


class Ngsepcore(MakefilePackage):
    """NGSEP provides an object model to enable different kinds of
analysis of DNA high throughput sequencing (HTS) data."""

    homepage = "https://sourceforge.net/projects/ngsep/"
    url      = "https://downloads.sourceforge.net/project/ngsep/SourceCode/NGSEPcore_4.0.0.tar.gz"
    list_url = "https://sourceforge.net/projects/ngsep/files/SourceCode"
    list_depth = 1

    def url_for_version(self, version):
        url = "https://downloads.sourceforge.net/project/ngsep/SourceCode/NGSEPcore_{0}.tar.gz"
        return url.format(version)

    # maintainers = ['github_user1', 'github_user2']
    version('4.0.3', sha256='15cf59f96e63f14fb215ec5211a21ca5f2295d62eabfa9b6bb1cff11c4ad1770')
    version('4.0.2', sha256='6b83fc78255d2978ac1c896b6913dac4a71cb8e6d3458f471436ab8345b84bf9')
    version('4.0.1', sha256='6d6f712dfb5237f3ff4cea15321348ffe606cfc44acecc04fe481e150fe71de8')
    version('4.0.0', sha256='545fb780b6ff29781e6d05840e94fa0367cafd80412d173860943093c8a8a4eb')

    depends_on('openjdk@1.8.0:1.8.999', type=('build', 'run'), when='@:4.0.1')
    depends_on('openjdk@11.0:11.99', type=('build', 'run'), when='@4.0.2:')

    parallel = False
    build_targets = ['all']

    def edit(self, spec, prefix):
        env['JAVA_TOOL_OPTIONS'] = '-Dfile.encoding=UTF8' 

    def install(self, spec, prefix):
        mkdirp(prefix.bin)
        jar_file = 'NGSEPcore_{0}.jar'.format(self.version.dotted)
        install(jar_file, prefix.bin)
        
        # Set up a helper script to call java on the jar file,
        # explicitly codes the path for java and the jar file.
        script_sh = join_path(os.path.dirname(__file__), "NGSEPcore.sh")
        script = join_path(prefix.bin, "NGSEPcore")
        install(script_sh, script)
        set_executable(script)

        # Munge the helper script to explicitly point to java and the
        # jar file.
        java = self.spec['openjdk'].prefix.bin.java
        kwargs = {'ignore_absent': False, 'backup': False, 'string': False}
        filter_file('^java', java, script, **kwargs)
        filter_file('jarpath', join_path(prefix.bin, jar_file), script, **kwargs)

