from spack import *

class OvisLdms(AutotoolsPackage):
    """OVIS is a modular system for HPC data collection, transport, storage, -log message exploration, and visualization as well as analysis.  LDMS is a low-overhead, low-latency framework for collecting, transfering, and storing metric data on a large distributed computer system."""

    homepage = "https://github.com/ovis-hpc/ovis"
    url      = "https://github.com/ovis-hpc/ovis/archive/OVIS-4.3.6.tar.gz"
    git      = "https://github.com/ovis-hpc/ovis.git"

    maintainers = ['nvonwolf']

    version('4.3.6', tag='OVIS-4.3.6', submodules=True)
    version('4.3.5', tag='OVIS-4.3.5', submodules=True)

    variant('papi', default=True, description='Require components that depend upon libpapi (and libpfm)')
    variant('sos', default=True, description='Enable the sos module')
    variant('appinfo', default=True, description='Enable the appinfo module')
    variant('appsampler', default=True, description='Enable the appsampler module')
    variant('slurm', default=True, description='Enable slurm module')

    # Generic Dependencies
    depends_on('autoconf@2.63:', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')
    depends_on('gmake', type='build')
    depends_on('doxygen', type='build')
    depends_on('bison', type=('build', 'link'))
    depends_on('flex', type=('build', 'link'))
    depends_on('readline', type=('build', 'link'))
    depends_on('openssl', type=('build', 'link'))
    depends_on('munge', type=('build', 'link'))
    depends_on('python@3.6:', type=('build', 'link'))
    depends_on('py-cython@0.25:', type=('build', 'link'))

    # Variant = papi
    depends_on('papi', when='+papi', type=('build', 'link'))
    depends_on('libpfm4', when='+papi', type=('build', 'link'))

    # Variant = sos
    depends_on('ovis-sos', when='+sos', type=('build', 'link'))

    # Variant = slurm
    depends_on('slurm', when='+slurm', type=('build', 'link'))

    def configure_args(self):
        args = []

        if self.spec.satisfies('+papi'):
            args.append('--enable-papi')
            args.append('--with-libpapi-prefix=' + self.spec['papi'].prefix)
            args.append('--with-libpfm-prefix=' + self.spec['libpfm4'].prefix)
        
        if self.spec.satisfies('+sos'):
            args.append('--enable-sos')
            args.append('--with-sos=' + self.spec['ovis-sos'].prefix)

        if self.spec.satisfies('+appinfo'):
            args.append('--enable-appinfo')

        if self.spec.satisfies('+appsampler'):
            args.append('--enable-app-sampler')

        if self.spec.satisfies('+slurm'):
            args.append('--with-slurm=' + self.spec['slurm'].prefix)

        return args

