from spack import *

class Ttyd(CMakePackage):
    """ttyd is a simple command-line tool for sharing terminal over the web."""

    homepage = "https://tsl0922.github.io/ttyd"
    url      = "https://github.com/tsl0922/ttyd/archive/1.6.3.tar.gz"
    git      = "https://github.com/tsl0922/ttyd.git"

    maintainers = ['nvonwolf']

    version('master', branch='master', submodules=True)
    version('1.6.3', sha256='1116419527edfe73717b71407fb6e06f46098fc8a8e6b0bb778c4c75dc9f64b9')
    version('1.6.2', sha256='fd3256099e1cc5c470220cbfbb3ab2c7fa1f92232c503f583556a8965aa83bac')
    version('1.6.1', sha256='d72dcca3dec00cda87b80a0a25ae4fee2f8b9098c1cdb558508dcb14fbb6fafc')

    depends_on('cmake', type='build')
    depends_on('gmake', type='build')
    depends_on('git', type='build')
    depends_on('autoconf', type='build')
    depends_on('autoconf-archive', type='build')
    depends_on('automake', type='build')
    depends_on('libtool', type='build')

    depends_on('file', type=('build', 'link'))
    depends_on('zlib', type=('build','link'))
    depends_on('mbedtls', type=('build','link'))
    depends_on('libuv', type=('build','link'))
    depends_on('libwebsockets', type=('build','link'))
    depends_on('json-c', type=('build','link'))
    depends_on('curl', type=('build','link'))

    def cmake_args(self):
        args = ['.']
        return args
