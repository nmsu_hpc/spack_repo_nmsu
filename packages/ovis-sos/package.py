from spack import *

class OvisSos(AutotoolsPackage):
    """SOS (pronuounced "sôs") -- Scalable Object Store -- is a high-performance, indexed, object-oriented database designed to efficiently manage structured data on persistent media."""

    homepage = "https://github.com/ovis-hpc/sos"
    url      = "https://github.com/ovis-hpc/sos/archive/OVIS-4.3.1.tar.gz"

    maintainers = ['nvonwolf']

    version('4.3.1', sha256='1224515749632485651eaf8ae64644ab443ec9cfe743ea7263e2a7e4676dd9b0')

    variant('python', default=True, description='Enable/Disable Python Command and interface libraries')
    variant('debug', default=False, description='Enable/Disable debugging logic inside libraries')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('python@3.6:', type=('build', 'link', 'test'))
    depends_on('py-cython', type=('build', 'link', 'test'))
    depends_on('py-numpy', type=('build', 'link', 'test'))


    def autoreconf(self, spec, prefix):
        mkdir = which('mkdir')
        mkdir('-p', join_path(self.stage.source_path, 'config'))
        mkdir('-p', join_path(self.stage.source_path, 'm4'))
        autoreconf('-v', '--force', '--install', '-I', 'm4')

    def configure_args(self):
        args = []
	
        if self.spec.satisfies('~python'):
            args.append('--disable-python')

        if self.spec.satisfies('+debug'):
            args.append('--enable-debug')

        return args

