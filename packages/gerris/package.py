from spack import *
import os


class Gerris(AutotoolsPackage):
    """Gerris is a Free Software program for the solution of the partial differential equations describing fluid flow."""

    homepage = "http://gfs.sourceforge.net/wiki/index.php/Main_Page"
    url      = "http://gerris.dalembert.upmc.fr/gerris/tarballs/gerris-snapshot-131206.tar.gz"

    maintainers = ['nvonwolf']

    version('131206', sha256='d5346a362b104ccc858c4b79938727d56c4654b103c268e54cf3aa56d0b55b39')

    variant('mpi', default=False, description='Enable MPI support')
    variant('hypre', default=False, description='Enable Hypre multigrid solver support')
    variant('map', default=True, description='Enable Map module support')
    variant('lis', default=False, description='Enable lis module support')
    variant('fft', default=True, description='Enable fft module support')

    conflicts('+hypre', when='~mpi')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type=('build', 'link'))

    depends_on('gcc', type=('build', 'link', 'run'))
    depends_on('hdf5 ~mpi', when='~mpi', type=('build')) # fix netcdf-c bring in openmpi through hdf5
    depends_on('netcdf-c ~mpi', when='~mpi', type=('build', 'link'))
    depends_on('gts', type=('build', 'link', 'run'))
    depends_on('glib', type=('build','link'))
    depends_on('zlib', type=('build', 'link'))
    depends_on('pcre+utf', type=('build', 'link'))
    depends_on('pkgconfig', type=('build', 'link'))
    depends_on('libffi', type=('build', 'link'))
    depends_on('libiconv', type=('build', 'link'))
    depends_on('gettext', type=('build', 'link'))
    depends_on('gsl', type=('build', 'link'))
    depends_on('ffmpeg +nonfree +shared', type=('build', 'link'))

    depends_on('proj@4.6.1:4.9.2', when='+map', type=('build', 'link'))

    depends_on('lis ~mpi', when='+lis~mpi', type=('build', 'link'))
    depends_on('fftw ~mpi', when='+fft~mpi', type=('build', 'link'))

    depends_on('netcdf-c +mpi +parallel-netcdf', when='+mpi', type=('build', 'link'))
    depends_on('hypre +mpi +shared', when='+hypre+mpi', type=('build', 'link'))
    depends_on('lis +mpi', when='+lis+mpi', type=('build', 'link'))
    depends_on('fftw +mpi', when='+fft+mpi', type=('build', 'link'))
    depends_on('mpi', when='+mpi', type=('build', 'link', 'run'))

    @run_before('configure')
    def configure_fixes(self):
        if '+mpi' in self.spec:
            filter_file(r'COMPILER\|\$CC', 'COMPILER|' + self.spec['mpi'].prefix.bin.mpicc, join_path(self.stage.source_path, 'configure'))
        else:
            filter_file(r'COMPILER\|\$CC', 'COMPILER|' + self.spec['gcc'].prefix.bin.gcc, join_path(self.stage.source_path, 'configure'))

        if '+hypre' in self.spec:
            os.environ['CPPFLAGS'] = '-I' + self.spec['hypre'].prefix.include + ' '
            os.environ['LDFLAGS'] = '-L' + self.spec['hypre'].prefix.lib + ' '
            # Below is what is in the official docs but it causes the configure pass to fail.
            #os.environ['LDFLAGS'] = '-L' + self.spec['hypre'].prefix.lib + ' -lmpi_cxx '


    def setup_run_environment(self, env):
        env.prepend_path('LD_LIBRARY_PATH', self.spec['gts'].prefix.lib)
        env.prepend_path('PKG_CONFIG_PATH', self.spec['gts'].prefix.lib.pkgconfig)


