from spack import *

class Gfsview(AutotoolsPackage):
    """FIXME: Put a proper description of your package here."""

    homepage = "http://gfs.sourceforge.net/wiki/index.php/Main_Page"
    url      = "http://gerris.dalembert.upmc.fr/gerris/tarballs/gfsview-snapshot-121130.tar.gz"

    maintainers = ['nvonwolf']

    version('121130', sha256='60440319e506d78f7bb5ebf63383f541e30d5e99ad05ea175876fb346e0766d9')

    variant('gtk', default=False, description='Enable GTK 3d GUI (currently broken)')
    variant('hypre', default=False, description='Enable Hypre multigrid solver support')

    depends_on('ftgl', type=('build', 'link'))
    depends_on('mesa', type=('build', 'link'))
    depends_on('mesa-glu', type=('build', 'link'))
    depends_on('gerris', type=('build', 'link'))
    depends_on('gts', type=('build', 'link'))

    # Hypre
    depends_on('hypre', when='+hypre', type=('build', 'link'))
   
    # GTK GUI
    depends_on('gtkplus@2:2.999.999', when='+gtk', type=('build', 'link'))
    depends_on('gtk-doc', when='+gtk', type=('build', 'link'))
    depends_on('gtkmm', when='+gtk', type=('build', 'link'))
    depends_on('startup-notification', when='+gtk', type=('build', 'link'))

    def configure_args(self):
        # FIXME: Add arguments other than --prefix
        # FIXME: If not needed delete this function
        args = []
        return args
